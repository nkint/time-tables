import React, { useEffect, useRef } from "react";
import { isNil } from "@thi.ng/checks";
import { createScene } from "./scene1/scene1";

type Scene = {
    createCanvas: (domContainer: HTMLDivElement) => void;
    dispose: () => void;
    Controls: React.FC;
};

export const Canvas: React.FC = () => {
    const domContainer = useRef<null | HTMLDivElement>(
        null
    );

    const scene = useRef<Scene>(createScene()).current;

    useEffect(() => {
        if (isNil(domContainer.current)) {
            console.log("Canvas react ref.current is nil");
        }
        scene.createCanvas(domContainer.current!);
        return () => scene.dispose();
    }, []);

    return (
        <div>
            <div
                className="fixed right-0 top-0 pa3"
                style={{ paddingTop: 90, float: "right" }}
            >
                <scene.Controls />
            </div>
            <div
                className="absolute ba top-0 left-0"
                ref={domContainer}
                style={{
                    width: "100vw",
                    height: "100vh",
                    zIndex: -1,
                }}
            ></div>
        </div>
    );
};

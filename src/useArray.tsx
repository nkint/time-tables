import { useCallback, useMemo, useState } from 'react';

export function useArray<T>() {
  const [value, setValue] = useState([] as T[]);
  const push = useCallback((a) => {
    setValue((v) => [...v, ...(Array.isArray(a) ? a : [a])]);
  }, []);
  const clear = useCallback(() => setValue(() => []), []);

  const actions = useMemo(
    () => ({
      push,
      clear,
    }),
    [push, clear]
  );
  return [value, actions] as [T[], typeof actions];
}

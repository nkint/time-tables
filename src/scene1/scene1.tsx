import React, { useEffect, useState } from 'react';
import { adaptDPI } from '@thi.ng/adapt-dpi';
import { fromRAF, sidechainToggle, stream, Subscription } from '@thi.ng/rstream';
import { circle, group } from '@thi.ng/geom';
import { draw } from '@thi.ng/hiccup-canvas';
import { fit } from '@thi.ng/math';
import { useForceUpdate } from '../force-update';

export const SliderInput: React.FC<{
  name: string;
  value: number;
  onChange: (num: number) => void;
}> = ({ name, value, onChange }) => {
  const [midiNumber, setMidiNumber] = useState('⌾');

  return (
    <div className="mv3">
      <div className="mb3">
        <label htmlFor={name}>{name}:</label>
        <span className="ml2">{value}</span>
      </div>
      <button className="b--none bg-white pointer" onClick={() => {}}>
        {midiNumber}
      </button>

      <input
        id={name}
        type="range"
        min={0}
        max={126}
        onChange={(event) => {
          const num = Number(event.target.value);
          onChange(num);
        }}
      />
    </div>
  );
};

export function createScene() {
  let domContainer: null | HTMLDivElement = null;
  let ctx: null | CanvasRenderingContext2D = null;
  let canvas: null | HTMLCanvasElement = null;
  let loop: Subscription<number, number>;
  const raf = fromRAF(); // 60Hz update loop
  const pause = stream();
  pause.next(false);
  const pausableRaf = raf.subscribe(sidechainToggle(pause));

  const state = { radius: 30 };

  const Controls: React.FC = () => {
    const [isPause, setIsPause] = useState(pause.deref());
    const update = useForceUpdate();

    // start with pause
    useEffect(() => {
      setIsPause(!isPause);
      pause.next(isPause);
    }, []);

    return (
      <div>
        <button
          onClick={() => {
            setIsPause(!isPause);
            pause.next(isPause);
          }}
        >
          {isPause ? 'play' : 'pause'}
        </button>
        <br />
        <SliderInput
          name={'radius'}
          value={state.radius}
          onChange={(num) => {
            state.radius = num;
            update();
          }}
        />
      </div>
    );
  };

  function createCanvas(_domContainer: HTMLDivElement) {
    console.log('create canvas');
    domContainer = _domContainer;

    const dimensions = domContainer.getBoundingClientRect();

    canvas = document.createElement('canvas');

    ctx = canvas.getContext('2d')!;
    domContainer.appendChild(canvas);
    adaptDPI(canvas, dimensions.width, dimensions.height);

    initLoop(ctx, dimensions);
  }

  function dispose() {
    console.log('dispose canvas');
    ctx!.clearRect(0, 0, canvas!.width, canvas!.height);
    domContainer!.removeChild(canvas!);
    disposeLoop();
  }

  function initLoop(ctx: CanvasRenderingContext2D, dimensions: { width: number; height: number }) {
    loop = pausableRaf.subscribe({
      next() {
        console.log('loop');

        ctx!.clearRect(0, 0, canvas!.width, canvas!.height);

        const c1 = circle(state.radius, {
          fill: 'red',
        });

        draw(
          ctx,
          group(
            {
              translate: [
                (dimensions.width * window.devicePixelRatio) / 2,
                (dimensions.height * window.devicePixelRatio) / 2,
              ],
            },
            [c1]
          )
        );
      },
    });
  }

  function disposeLoop() {
    console.log('dispose loop');
    loop.unsubscribe();
  }

  return {
    createCanvas,
    dispose,
    initLoop,
    disposeLoop,
    Controls,
  };
}

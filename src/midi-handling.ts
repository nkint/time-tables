import { isNil } from '@thi.ng/checks';
import WebMidi from 'webmidi';
import type {
  Input,
  WebMidiEvents,
  InputEventMidimessage,
  WebMidiEventConnected,
  WebMidiEventDisconnected,
  MidiPort,
} from 'webmidi';

const RECEIVE_MSG_TYPES = 'midimessage'; // only receive sysex messages
const ERROR_MIDI_NOT_SUPPORTED = 1;
const ERROR_MIDI_NOT_ENABLED = 2;
const ERROR_UNKNOWN = 3;

export type GenericError = {
  code: number;
  message: string;
};

export type MidiError = string | Error | GenericError;
export type MidiEvent = WebMidiEvents | InputEventMidimessage;

export type MidiState = {
  addError: (err: MidiError) => void;
  addEvent: (evt: MidiEvent) => void;
  resetErrors: () => void;
  resetEvents: () => void;
};

export class MidiHandler {
  midiState: null | MidiState = null;

  constructor(midiState: MidiState) {
    console.log('............................. ctor');
    if (isNil(midiState)) {
      throw new Error('MidiState is nil');
    }

    this.midiState = midiState;
  }

  init() {
    if (WebMidi.enabled) {
      console.log(`Midi.init: already enabled, register ports`);
      for (let port of WebMidi.inputs) {
        this.autoConnectInput(port);
      }
    } else {
      console.log('Midi.init: Calling WebMidi.enable');
      WebMidi.enable(this.midiOn, true);
    }
  }

  dispose() {
    console.log('Midi.dispose: unregister all inputs');
    for (let port of WebMidi.inputs) {
      this.disconnectInput(port);
    }

    this.midiState!.resetErrors();
    this.midiState!.resetEvents();
  }

  autoConnectInput = (port: Input) => {
    console.log(`Midi.autoConnectInput ${port.id} ?`);
    this.connectInput(port);
  };

  connectInput = (port: Input) => {
    if (port) {
      console.log(`Midi.connectInput: connect input ${port.id}`);

      if (port.hasListener(RECEIVE_MSG_TYPES, 'all', this.handleMidiInputEvent)) {
        console.warn(`Midi.connectInput: listener already connected`);
      } else {
        console.log(`Midi.connectInput: add listener for all channels`);
        port.addListener(RECEIVE_MSG_TYPES, 'all', this.handleMidiInputEvent);
      }
    }
    //TODO: error if port null
  };

  handleMidiInputEvent = (evt: InputEventMidimessage) => {
    console.log('handleMidiInputEvent', evt);
    if (evt.data[0] === 0xf8) {
      // we ignore Timing Clock messages
      return;
    }
    // e.data is UInt8Array
    this.midiState!.addEvent(evt);
  };

  midiOn = (err: undefined | Error) => {
    console.log('webmidi.supported=', WebMidi.supported);
    console.log('webmidi.enabled=', WebMidi.enabled);

    if (err) {
      console.warn('Midi.midiOn: WebMidi could not be enabled.', err);
      console.log(`Midi.midiOn: err.name=${err.name} err.message=${err.message}`);

      const code = !WebMidi.supported
        ? ERROR_MIDI_NOT_SUPPORTED
        : !WebMidi.enabled
        ? ERROR_MIDI_NOT_ENABLED
        : ERROR_UNKNOWN;

      return this.midiState!.addError({ code, message: err.message });
    }

    console.log('Midi.midiOn: WebMidi enabled');
    WebMidi.addListener('connected', this.handleMidiConnectEvent);
    WebMidi.addListener('disconnected', this.handleMidiConnectEvent);
  };

  disconnectInput = (port: Input | MidiPort) => {
    if (port) {
      console.log(`Midi.disconnectInput: disconnect input ${port.id}`);
      if ((port as any).removeListener) {
        (port as any).removeListener();
      }
    }
    //TODO: error if port null
  };

  handleMidiConnectEvent = (evt: WebMidiEventConnected | WebMidiEventDisconnected) => {
    if (evt.port.type === 'input') {
      console.log(
        `handleMidiConnectEvent: ${evt.type} port: ${evt.port.type} ${evt.port.connection} ${evt.port.state}: ${evt.port.name} ${evt.port.id}`,
        evt
      );
      if (evt.type === 'disconnected') {
        this.disconnectInput(evt.port);
      } else {
        console.log('Midi.handleMidiConnectEvent: call addInput');
        this.autoConnectInput(evt.port);
      }
    }
  };
}

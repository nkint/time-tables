import React, { useState } from 'react';
import { render } from 'react-dom';
import { Canvas } from './Canvas';

import { MidiState } from './midi-handling';
import { useMidi } from './useMidi';

const Help: React.FC = () => {
  return <div>👋</div>;
};

const App: React.FC = () => {
  const [isHelp, setIsHelp] = useState(false);

  const { midiHandler, midiErrors, midiEvents } = useMidi();

  return (
    <div>
      <header className="w-100 h2 bb b--silver bg-near-white">
        <button
          onClick={() => {
            console.log('click');
            setIsHelp(!isHelp);
          }}
        >
          click to help
        </button>
      </header>

      {isHelp ? <Help /> : <Canvas />}

      <div className="ma3">
        <h2>Midi status</h2>
        <button
          onClick={() => {
            midiHandler.dispose();
            midiHandler.init();
          }}
        >
          Re-initialize Midi
        </button>

        <h3>errors</h3>
        <ul>
          {midiErrors.map((err, i) => (
            <li key={i}>{JSON.stringify(err)}</li>
          ))}
        </ul>

        <h3>events</h3>
        <ul>
          {midiEvents.map((evt, i) => (
            <li key={i}>{JSON.stringify(evt)}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

render(<App />, document.getElementById('root'));

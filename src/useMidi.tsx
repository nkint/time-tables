import { useEffect, useRef } from 'react';
import { MidiError, MidiEvent, MidiHandler } from './midi-handling';
import { useArray } from './useArray';

export function useMidi() {
  const [midiErrors, midiErrorsActions] = useArray<MidiError>();
  const [midiEvents, midiEventsActions] = useArray<MidiEvent>();

  const midiHandler = useRef<MidiHandler | null>(null);

  useEffect(() => {
    const instance = new MidiHandler({
      addEvent: midiEventsActions.push,
      resetEvents: midiEventsActions.clear,
      addError: midiErrorsActions.push,
      resetErrors: midiErrorsActions.clear,
    });

    console.log('call init');
    midiHandler.current = instance;
    midiHandler.current.init();

    return () => midiHandler.current?.dispose();
  }, [midiHandler, midiErrorsActions, midiEventsActions]);

  return { midiHandler: midiHandler.current!, midiErrors, midiEvents };
}
